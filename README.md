# solaredge-setapp-ocaml

## Dependencies

* OCaml >=4.05.0 (the compiler),
* GNU make (to build the code),
* `ocaml-protoc` and ocaml-protoc-yojson (to process the the proto files).

## Getting started

```console
$ opam switch create . --deps-only
$ opam install ocaml-protoc ocaml-protoc-yojson
$ make opt  # or make byte
```

Test it:
```sh
./solaredge-setapp-ocaml.{opt,run} -src http://172.16.0.1 -ep status
```
