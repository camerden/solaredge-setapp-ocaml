open Lwt
open Cohttp
open Cohttp_lwt_unix


let recv_buf url =
  let body url =
    Client.get (Uri.of_string url) >>= fun (resp, body) ->

    let code = resp |> Response.status |> Code.code_of_status in
    if code <> 200 then
      raise (Failure ("response code: " ^ Int.to_string code));

    match Header.get (Response.headers resp) "content-type" with
    | None -> raise (Failure "missing content-type header")
    | Some ctype ->
       if ctype <> "application/x-protobuf" then
         raise (Failure ("invalid content-type: " ^ ctype));

       Cohttp_lwt.Body.to_string body
  in
  Pbrt.Decoder.of_string (Lwt_main.run (body url))
