let user_agent = "solaredge-setapp-ocaml/0.1"

type http_version = [`HTTP_1_0 | `HTTP_1_1]

let http_label = function
  | "HTTP/1.0" -> `HTTP_1_0
  | "HTTP/1.1" -> `HTTP_1_1
  | _ -> failwith "invalid HTTP version"

module LCString = struct
  type t = string
  let of_string s = String.lowercase_ascii s
  let to_string s = s
  let compare s1 s2 = String.compare s1 s2
end

module HeaderMap = Map.Make(LCString)

type http_response = {
    headers: string HeaderMap.t;
    version: http_version;
    status: int;
    body: bytes;
  }

module Http_response = struct
  type http_status = {
      http_version: http_version;
      code: int;
      message: string;
    }

  type t = http_response

  let split_http_header s =
    match Str.(bounded_split (regexp ":[ \t]*") s 2) with
    | k :: v :: [] -> [k; v]
    | x -> x

  let parse_status_line ic =
    let line = input_line ic in
    Scanf.sscanf (line ^ "\r\n") "%s %u %[^\r\n]\r\n"
      (fun http_vers code msg ->
        { http_version = http_label http_vers;
          code = code;
          message = msg;
        })

  let parse_http_headers ic =
    let rec parse' headers =
      match split_http_header (input_line ic) with
      | k :: v :: [] ->
         parse' (HeaderMap.add (LCString.of_string k) v headers)
      | _ -> headers
    in
    parse' HeaderMap.empty

  let buffer_of_channel ic =
    let b = Buffer.create 4096 in
    begin
      try
        while true do
          Buffer.add_channel b ic 1024
        done
      with End_of_file -> ()
    end;
    b

  let of_channel ic =
    let status = parse_status_line ic in
    let headers = parse_http_headers ic in
    let body = buffer_of_channel ic in
    { headers = headers;
      version = status.http_version;
      status = status.code;
      body = Buffer.to_bytes body;
    }
end

type uri_parts = {
    scheme: string;
    host: string;
    port: int;
    path_and_query: string;
  }

let make_uri_parts s =
  if not Str.(string_match (regexp "^\\([a-z]+://\\)\\([^:/]+\\)\\(:[0-9]+\\)?\\(.*\\)$") s 0) then
    failwith ("Invalid URI: " ^ s);
  { scheme = (
      match Str.matched_group 1 s with
      | s -> String.(sub s 0 @@ length s - 3)
      | exception Not_found -> "http"
    );
    host = (
      match Str.matched_group 2 s with
      | s -> s
      | exception Not_found -> failwith "Missing host in URL"
    );
    port = (
      match Str.matched_group 3 s with
      | s -> int_of_string String.(sub s 1 @@ length s - 1)
      | exception Not_found -> 80
    );
    path_and_query = Str.matched_group 4 s;
  }

let recv_buf url =
  let uri_parts = make_uri_parts url in
  if uri_parts.scheme <> "http" then
    failwith "Only http:// connections are supported";
  let ip = Unix.((gethostbyname uri_parts.host).h_addr_list.(0)) in
  let addr = Unix.ADDR_INET (ip, uri_parts.port) in
  let sock = Unix.(socket PF_INET SOCK_STREAM 0) in
  let body =
    try
      Unix.connect sock addr;
      let ic = Unix.in_channel_of_descr sock in
      let oc = Unix.out_channel_of_descr sock in
      output_string oc ("GET " ^ uri_parts.path_and_query ^ " HTTP/1.1\r\n");
      output_string oc ("Host: " ^ uri_parts.host ^ "\r\n");
      output_string oc ("User-Agent: " ^ user_agent ^ "\r\n");
      output_string oc ("Connection: close\r\n");
      output_string oc ("\r\n");
      flush oc;

      let resp = Http_response.of_channel ic in
      (* TODO: checks *)
      resp.body
    with e -> Unix.close sock; raise e
  in
  Pbrt.Decoder.of_bytes body
