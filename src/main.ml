let gen_url host ep =
  match ep with
  | "information" -> host ^ "/web/v1/information"
  | "maintenance" -> host ^ "/web/v1/maintenance"
  | "status" -> host ^ "/web/v1/status"
  | ep -> raise (Arg.Bad("Invalid endpoint: " ^ ep))


let matches_re r s =
  Str.(string_match (regexp r) s 0)


let recv src ep =
  match src with
  | url when matches_re "^https?://" url ->
      Http.recv_buf (gen_url url ep)
  | file_uri when matches_re "^file://" file_uri ->
      File.read_buf String.(sub file_uri 7 (length file_uri - 7))
  | path when Sys.file_exists path ->
      File.read_buf path
  | src -> failwith ("Invalid -src: " ^ src)


let () =
  let src = ref "" in
  let ofmt = ref "json" in
  let ep = ref "" in
  Arg.parse Arg.[
      ("-src", Set_string src, "source file or http URL");
      ("-ofmt", Set_string ofmt, ("the output format (default: " ^ !ofmt ^ ")"));
      ("-ep", Set_string ep, "endpoint to query")
    ] (fun s -> ()) "usage";

  let api = match !ep with
    | "information" -> new Endpoints.information_api (recv !src "information")
    | "maintenance" -> new Endpoints.maintenance_api (recv !src "maintenance")
    | "status" -> new Endpoints.status_api (recv !src "status")
    | s -> failwith ("Invalid endpoint: " ^ s)
  in
  match !ofmt with
  | "json" ->
     api#to_json |> print_endline
  | ofmt -> failwith ("Invalid output format: " ^ ofmt)
