class virtual api_endpoint pb =
  object
    method virtual to_json : string
  end

class information_api pb =
  object
    inherit api_endpoint pb

    val information = Information_pb.decode_information pb

    method to_json =
      Information_yojson.encode_information information
      |> Yojson.Basic.pretty_to_string
  end

class maintenance_api pb =
  object
    inherit api_endpoint pb

    val maintenance = Maintenance_pb.decode_maintenance pb

    method to_json =
      Maintenance_yojson.encode_maintenance maintenance
      |> Yojson.Basic.pretty_to_string
  end

class status_api pb =
  object
    inherit api_endpoint pb

    val status = Status_pb.decode_status pb

    method to_json =
      Status_yojson.encode_status status
      |> Yojson.Basic.pretty_to_string
  end
