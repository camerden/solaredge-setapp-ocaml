let read_buf path =
  let bytes =
    let ic = open_in path in
    let len = in_channel_length ic in
    let bytes = Bytes.create len in
    really_input ic bytes 0 len;
    close_in ic;
    bytes
  in
  Pbrt.Decoder.of_bytes bytes
