MESSAGES_VERSION = 1.4.10

OCAMLFIND = ocamlfind
OCAMLOPT = ocamlopt
OCAMLC = ocamlc
BCC = $(OCAMLFIND) $(OCAMLC) $(BCFLAGS)
OPTC = $(OCAMLFIND) $(OCAMLOPT) $(OPTFLAGS)


PACKAGES = str unix yojson ocaml-protoc ocaml-protoc-yojson

ENDPOINTS = information maintenance status
SRCDIR = src
MODULES = file.ml http.ml endpoints.ml main.ml

SRCS = $(MODULES:%=$(SRCDIR)/%)

BIN = solaredge-setapp-ocaml


PROTO_DIR = proto/$(MESSAGES_VERSION)
MESSAGES_SRCDIR = $(SRCDIR)/messages
MESSAGES_STEMS = $(ENDPOINTS:%=%_types) $(ENDPOINTS:%=%_pb) $(ENDPOINTS:%=%_pp) $(ENDPOINTS:%=%_yojson)
MESSAGES_ML = $(MESSAGES_STEMS:%=$(MESSAGES_SRCDIR)/%.ml)
MESSAGES_MLI = $(MESSAGES_STEMS:%=$(MESSAGES_SRCDIR)/%.mli)


.PHONY: byte native opt
byte: $(BIN).run
native opt: $(BIN).opt


# Generic

.SUFFIXES: .ml .mli .cmi .cmo .cmx

.ml.cmo:
	$(BCC) -package '$(PACKAGES)' -I $(SRCDIR) -I $(MESSAGES_SRCDIR) -c - $(filter-out %.cmi,$^)
.ml.cmx:
	$(OPTC) -package '$(PACKAGES)' -I $(SRCDIR) -I $(MESSAGES_SRCDIR) -c - $(filter-out %.cmi,$^)
.mli.cmi:
	$(BCC) -package '$(PACKAGES)' -I $(SRCDIR) -I $(MESSAGES_SRCDIR) -c - $<


# Protocol Buffers

$(MESSAGES_SRCDIR):
	mkdir $(MESSAGES_SRCDIR)

.PRECIOUS: $(MESSAGES_ML) $(MESSAGES_MLI)
$(MESSAGES_SRCDIR)/%_types.mli \
$(MESSAGES_SRCDIR)/%_types.ml \
$(MESSAGES_SRCDIR)/%_pb.mli \
$(MESSAGES_SRCDIR)/%_pb.ml \
$(MESSAGES_SRCDIR)/%_pp.mli \
$(MESSAGES_SRCDIR)/%_pp.ml \
$(MESSAGES_SRCDIR)/%_yojson.mli \
$(MESSAGES_SRCDIR)/%_yojson.ml: $(PROTO_DIR)/%.proto | $(MESSAGES_SRCDIR)
	ocaml-protoc -binary -pp -yojson -ml_out $(MESSAGES_SRCDIR) $(PROTO_DIR)/$*.proto
	sed -i -e 's/Yojson\.Basic\.json/Yojson.Basic.t/' $(MESSAGES_SRCDIR)/$*_yojson.mli

# .PRECIOUS: $(MESSAGES_MLI:%.mli=%.cmi)
# $(MESSAGES_SRCDIR)/%_types.cmi: $(MESSAGES_SRCDIR)/%_types.mli
# 	$(BCC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_types.mli
# $(MESSAGES_SRCDIR)/%_pb.cmi: $(MESSAGES_SRCDIR)/%_pb.mli
# 	$(BCC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_pb.mli
# $(MESSAGES_SRCDIR)/%_pp.cmi: $(MESSAGES_SRCDIR)/%_pp.mli
# 	$(BCC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_pp.mli
# $(MESSAGES_SRCDIR)/%_yojson.cmi: $(MESSAGES_SRCDIR)/%_yojson.mli
# 	$(BCC) -package ocaml-protoc -package yojson -package ocaml-protoc-yojson -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_yojson.mli

# .PRECIOUS: $(MESSAGES_ML:%.ml=%.cmx)
# $(MESSAGES_SRCDIR)/%_types.cmx: $(MESSAGES_SRCDIR)/%_types.ml $(MESSAGES_SRCDIR)/%_types.cmi
# 	$(OPTC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_types.ml
# $(MESSAGES_SRCDIR)/%_pb.cmx: $(MESSAGES_SRCDIR)/%_types.cmx $(MESSAGES_SRCDIR)/%_pb.cmi
# 	$(OPTC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_types.cmx $(MESSAGES_SRCDIR)/$*_pb.ml
# $(MESSAGES_SRCDIR)/%_pp.cmx: $(MESSAGES_SRCDIR)/%_types.cmx $(MESSAGES_SRCDIR)/%_pp.cmi
# 	$(OPTC) -package ocaml-protoc -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_types.cmx $(MESSAGES_SRCDIR)/$*_pp.ml
# $(MESSAGES_SRCDIR)/%_yojson.cmx: $(MESSAGES_SRCDIR)/%_types.cmx $(MESSAGES_SRCDIR)/%_yojson.cmi
# 	$(OPTC) -package yojson -package ocaml-protoc-yojson -I $(MESSAGES_SRCDIR) -c - $(MESSAGES_SRCDIR)/$*_types.cmx $(MESSAGES_SRCDIR)/$*_yojson.ml


# Executable

$(BIN).opt: $(MESSAGES_ML:%.ml=%.cmx) $(SRCS:%.ml=%.cmx)
	$(OPTC) -linkpkg $(PACKAGES:%=-package %) -I $(SRCDIR) -I $(MESSAGES_SRCDIR) -o $@ - $^

$(BIN).run: $(MESSAGES_ML:%.ml=%.cmo) $(SRCS:%.ml=%.cmo)
	$(BCC) -linkpkg $(PACKAGES:%=-package %) -I $(SRCDIR) -I $(MESSAGES_SRCDIR) -o $@ - $^


.PHONY: clean
clean:
	rm -f $(SRCDIR)/*.cmi $(SRCDIR)/*.cmo $(SRCDIR)/*.cmx $(SRCDIR)/*.o
	rm -f -R $(MESSAGES_SRCDIR)
	rm -f $(BIN).run $(BIN).opt
	rm -f $(SRCDIR)/.depend

$(SRCDIR)/.depend: $(MESSAGES_ML) $(MESSAGES_MLI) $(SRCS)
	ocamlfind ocamldep -all -I $(SRCDIR) -I $(MESSAGES_SRCDIR) $(SRCS) $(SRCS:%.ml=%.mli) $(MESSAGES_ML) $(MESSAGES_MLI) >$@


-include $(SRCDIR)/.depend
